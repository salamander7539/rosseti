import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';
import 'package:rosseti/screens/auth_screens/ui/welcome_screen.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view_1.dart';
import 'package:rosseti/screens/home_screen/ui/home_screen.dart';
import 'package:rosseti/screens/projects_screens/ui/projects_screen.dart';
import 'package:rosseti/screens/projects_screens/ui/projects_screen_view.dart';
import 'package:rosseti/screens/status_screen/ui/status_screen.dart';
import 'package:rosseti/splash_screen.dart';
import 'core/locator.dart';

class App extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  @override
  Future<void> initState() {
    // TODO: implement initState
    locator.get<AuthenticationManager>().init();
    super.initState();
  }



  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    locator.get<SecureStorage>().close();
    locator.reset(dispose: true);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => locator.get<NetworkInfo>()),
        BlocProvider(create: (_) => locator.get<AuthenticationManager>()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child,
          );
        },
        home: SplashScreen(),
      ),
    );
  }
}
