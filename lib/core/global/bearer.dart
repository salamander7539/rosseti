import 'dart:convert';

import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';

import '../locator.dart';

Future<String> getBearerToken() async {
  SecureStorage secureStorage = locator.get<SecureStorage>();

  final rawUser = await secureStorage.getData('MainUser');

  final VerificationSMSData smsData = VerificationSMSData.fromJson(json.decode(rawUser));

  final String bearerToken = smsData.token;

  return bearerToken;
}