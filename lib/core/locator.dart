import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/core/services/secure_storage.dart';

final locator = GetIt.instance;

void locatorSetup() {
 locator.registerLazySingleton<FlutterSecureStorage>(() => FlutterSecureStorage());
 locator.registerSingleton<SecureStorage>(SecureStorageImpl(storage: locator.get<FlutterSecureStorage>()));
 locator.registerLazySingleton<DataConnectionChecker>(() => DataConnectionChecker());
 locator.registerLazySingleton<NetworkInfo>(() => NetworkInfo(connectionChecker: locator.get<DataConnectionChecker>()));
 locator.registerSingleton<AuthenticationManager>(AuthenticationManager(secureStorage: locator.get<SecureStorage>()));
}