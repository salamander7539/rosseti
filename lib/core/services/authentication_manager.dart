import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';

class AuthenticationManager extends Cubit<bool> {
  final SecureStorage secureStorage;

  AuthenticationManager({@required this.secureStorage}) : assert(secureStorage != null), super(false);

  Future init() async {
    final rawUser = await secureStorage.getData('MainUser');
    if(rawUser != null){
      final VerificationSMSData mainUser = VerificationSMSData.fromJson(json.decode(rawUser));
      print('TOKEN __ ${mainUser.token}');
      if (mainUser?.token != null) emit(true);
    }
  }

  Future <bool> getUserToken() async {
    bool flag;
    String token;
    final rawUser = await secureStorage.getData('MainUser');
    print("RAW ${rawUser.toString()}");
    if (rawUser != null) {
      final VerificationSMSData mainUser = VerificationSMSData.fromJson(json.decode(rawUser));
      token = mainUser.token;
      flag = true;
    } else {
      flag = false;
    }
    print(flag);
    return flag;
  }

  void activate() => emit(true);

  void deactivate() => emit(false);

  bool get isAuth => state;
}

