import 'package:bloc/bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';


@lazySingleton
class NetworkInfo extends Cubit<bool> {
  final DataConnectionChecker connectionChecker;

  NetworkInfo({@required this.connectionChecker}) : assert(connectionChecker != null), super(true) {
    connectionChecker.onStatusChange.listen((status) {
      if(status == DataConnectionStatus.connected) {
        print('Network connected');
        emit(true);
      } else {
        print('Network disconnected');
        emit(false);
      }
    });
  }
  Future<bool> get isConnected => connectionChecker.hasConnection;
}