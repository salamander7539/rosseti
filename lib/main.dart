import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/secure_storage.dart';

import 'app.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white, // Color for Android
      statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
  ));

  WidgetsFlutterBinding.ensureInitialized();
  locatorSetup();
  locator.get<SecureStorage>().init();

  runApp(App());
}


