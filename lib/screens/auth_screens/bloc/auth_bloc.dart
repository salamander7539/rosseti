import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/screens/auth_screens/models/auth_phone_model.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';
import 'package:rosseti/screens/auth_screens/repository/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

abstract class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(AuthState initialState) : super(initialState);
}

class AuthBlocImpl extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;
  final AuthenticationManager authenticationManager;

  AuthBlocImpl({this.authRepository, this.authenticationManager}) : assert(authRepository != null), assert(authenticationManager != null), super(AuthInitialState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    print(authenticationManager.isAuth);
    if (event is SendPhone) {
      try {
        final AuthPhoneData authPhoneData = await authRepository.authData(phone: event.phone);
        yield AuthPhoneSuccessState(phoneData: authPhoneData);
      } on Exception catch (e) {
        yield AuthErrorState(e.toString());
      }
    }
    if (event is SendSMS) {
      try {
        final VerificationSMSData smsData = await authRepository.fetchData(phone: event.phone, code: event.code);

        print("check auth______ ${authenticationManager == null}");
        if (authenticationManager != null) authenticationManager.activate();
        print("Auth ${authenticationManager.isAuth}");
        yield VerifySMSSuccessState(smsData: smsData);

      } on Exception catch (e) {
        yield AuthErrorState(e.toString());
      }
    }
  }
}
