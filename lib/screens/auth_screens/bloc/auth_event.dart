part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class SendPhone extends AuthEvent {
  final String phone;

  const SendPhone({this.phone});

  @override
  // TODO: implement props
  List<Object> get props => [phone];
}

class SendSMS extends AuthEvent {
  final String phone;
  final int code;
  final String token;

  const SendSMS({this.phone, this.code, this.token});

  @override
  // TODO: implement props
  List<Object> get props => [phone, code, token];
}

class SetError extends AuthEvent {
  final String error;

  const SetError(this.error);

  @override
  // TODO: implement props
  List<Object> get props => [error];
}