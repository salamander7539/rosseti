part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitialState extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthPhoneSuccessState extends AuthState {
  final AuthPhoneData phoneData;

  const AuthPhoneSuccessState({this.phoneData});

  @override
  // TODO: implement props
  List<Object> get props => [phoneData];
}

class VerifySMSSuccessState extends AuthState {
  final VerificationSMSData smsData;

  const VerifySMSSuccessState({this.smsData});

  @override
  // TODO: implement props
  List<Object> get props => [smsData];

}

class AuthErrorState extends AuthState {
  final String error;

  const AuthErrorState(this.error);

  @override
  List<Object> get props => [error];
}
