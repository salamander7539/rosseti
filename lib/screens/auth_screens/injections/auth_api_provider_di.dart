
import 'package:rosseti/screens/auth_screens/provider/auth_api_provider.dart';

class AuthApiProviderInject {
  static AuthApiProvider _authApiProvider;

  AuthApiProviderInject._();

  static AuthApiProvider authApiProvider() {
    if(_authApiProvider == null) {
      _authApiProvider = AuthApiProviderImpl();
    }
    return _authApiProvider;
  }
}