import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/provider/auth_db_provider.dart';

class AuthDBProviderInject {
  static AuthDBProvider _authDBProvider;

  AuthDBProviderInject._();

  static AuthDBProvider authDBProvider() {
    if (_authDBProvider == null) {
      _authDBProvider = AuthDBProviderImpl(secureStorage: locator.get<SecureStorage>());
    }
    return _authDBProvider;
  }
}