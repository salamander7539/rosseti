import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/auth_screens/injections/auth_api_provider_di.dart';
import 'package:rosseti/screens/auth_screens/injections/auth_db_provider_di.dart';
import 'package:rosseti/screens/auth_screens/repository/auth_repository.dart';

class AuthRepositoryInject {
  static AuthRepository _authRepository;

  AuthRepositoryInject._();

  static AuthRepository authRepository() {
    if (_authRepository == null) {
      _authRepository = AuthRepositoryImpl(
          apiProvider: AuthApiProviderInject.authApiProvider(),
          authDBProvider: AuthDBProviderInject.authDBProvider(),
          networkInfo: locator.get<NetworkInfo>(),
      );
    }
    return _authRepository;
  }
}
