class AuthPhoneData {
  AuthPhoneData({
    this.success,
    this.code,
  });

  bool success;
  String code;

  factory AuthPhoneData.fromJson(Map<String, dynamic> json) => AuthPhoneData(
    success: json["success"],
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "code": code,
  };
}