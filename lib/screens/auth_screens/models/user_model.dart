// To parse this JSON data, do
//
//     final userData = userDataFromJson(jsonString);

import 'dart:convert';

UserData userDataFromJson(String str) => UserData.fromJson(json.decode(str));

String userDataToJson(UserData data) => json.encode(data.toJson());

class UserData {
  UserData({
    this.id,
    this.fullName,
    this.phone,
    this.topicId,
    this.email,
    this.commentsCount,
    this.ratingsCount,
    this.acceptedProposalsCount,
    this.deniedProposalsCount,
    this.proposalsCount,
  });

  int id;
  String fullName;
  String phone;
  dynamic topicId;
  dynamic email;
  int commentsCount;
  int ratingsCount;
  int acceptedProposalsCount;
  int deniedProposalsCount;
  int proposalsCount;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
    id: json["id"],
    fullName: json["full_name"],
    phone: json["phone"],
    topicId: json["topic_id"],
    email: json["email"],
    commentsCount: json["comments_count"],
    ratingsCount: json["ratings_count"],
    acceptedProposalsCount: json["accepted_proposals_count"],
    deniedProposalsCount: json["denied_proposals_count"],
    proposalsCount: json["proposals_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "full_name": fullName,
    "phone": phone,
    "topic_id": topicId,
    "email": email,
    "comments_count": commentsCount,
    "ratings_count": ratingsCount,
    "accepted_proposals_count": acceptedProposalsCount,
    "denied_proposals_count": deniedProposalsCount,
    "proposals_count": proposalsCount,
  };
}
