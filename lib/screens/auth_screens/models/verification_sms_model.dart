class VerificationSMSData {
  VerificationSMSData({
    this.success,
    this.token,
  });

  bool success;
  String token;

  factory VerificationSMSData.fromJson(Map<String, dynamic> json) => VerificationSMSData(
    success: json["success"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "token": token,
  };
}