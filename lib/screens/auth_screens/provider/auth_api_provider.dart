import 'package:dio/dio.dart';
import 'package:rosseti/core/global/api.dart';
import 'package:rosseti/core/global/bearer.dart';
import 'package:rosseti/screens/auth_screens/models/auth_phone_model.dart';
import 'package:rosseti/screens/auth_screens/models/user_model.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';

abstract class AuthApiProvider<T> {
  Future<T> authPhone({String phone});

  Future<T> verifySms({String phone, int code});

  Future<T> getUser();
}

class AuthApiProviderImpl implements AuthApiProvider {
  final Dio _dio = Dio();

  @override
  Future authPhone({String phone}) async {
    try {
      Response response = await _dio.post("$apiUrl/auth/phone", data: {
        'phone': phone,
      });
      if (response.statusCode == 200) {
        print(response.data);
        return AuthPhoneData.fromJson(response.data);
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  @override
  Future verifySms({String phone, int code}) async {
    try {
      Response response = await _dio.post('$apiUrl/auth/verify-code',
          data: {'phone': phone, 'code': code});
      if (response.statusCode == 200) {
        print(response.data);
        return VerificationSMSData.fromJson(response.data);
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  @override
  Future getUser() async {
    try {
      String bearer = await getBearerToken();

      Response response = await _dio.get(
        '$apiUrl/user',
        options: Options(
          headers: {
            'Authorization': 'Bearer $bearer'
          },
        ),
      );
      if (response.statusCode == 200) {
        print(response.data);
        return UserData.fromJson(response.data);
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }
}
