import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/models/user_model.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';

abstract class AuthDBProvider<T> {
  Future<String> getToken();



  Future storeUser({
    T object,
    String token,
    int id,
    String fullName
  });

  Future removeUser();

}

class AuthDBProviderImpl implements AuthDBProvider {
  final SecureStorage secureStorage;

  AuthDBProviderImpl({@required this.secureStorage})
      : assert(secureStorage != null);

  @override
  Future<String> getToken() async {
    return await secureStorage.getData('MainUser');
  }

  // @override
  // Future<String> getRefreshToken() async {
  //   final mainUserString = await secureStorage.getData('MainUser');
  //   final VerificationSMSData mainUser =
  //       VerificationSMSData.fromJson(json.decode(mainUserString));
  //   return mainUser.token;
  // }

  @override
  Future storeUser({dynamic object, String token, int id, String fullName}) async {
    final row = await secureStorage.getData("MainUser");
    final rowUser = await secureStorage.getData("UserData");

    final prevUser = (row == null) ? null : VerificationSMSData.fromJson(json.decode(await secureStorage.getData('MainUser')));

    final prevData = (rowUser == null) ? null : UserData.fromJson(json.decode(await secureStorage.getData('UserData')));

    final VerificationSMSData mainUser = VerificationSMSData(
        token: token ?? prevUser.token,
    );

    final UserData userData = UserData(
      id: id ?? prevData.id,
      fullName: fullName ?? prevData.fullName,
    );

    secureStorage.putData('MainUser', json.encode(mainUser.toJson()));
    secureStorage.putData('UserData', json.encode(userData.toJson()));
  }

  @override
  Future removeUser() async {
    print("removeUser");
    secureStorage.removeData('MainUser');
    secureStorage.removeData('UserData');
  }
}
