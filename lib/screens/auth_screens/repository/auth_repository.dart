import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/auth_screens/models/auth_phone_model.dart';
import 'package:rosseti/screens/auth_screens/models/user_model.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';
import 'package:rosseti/screens/auth_screens/provider/auth_api_provider.dart';
import 'package:rosseti/screens/auth_screens/provider/auth_db_provider.dart';

abstract class AuthRepository<T> {
  Future<T> authData({String phone});
  Future<T> fetchData({String phone, int code});
}

class AuthRepositoryImpl implements AuthRepository {

  final AuthApiProvider apiProvider;
  final AuthDBProvider authDBProvider;
  final NetworkInfo networkInfo;

  AuthRepositoryImpl({@required this.apiProvider, @required this.networkInfo, @required this.authDBProvider}) : assert(apiProvider != null), assert(authDBProvider != null), assert(networkInfo != null);


  @override
  Future authData({String phone}) async {
    if(await networkInfo.isConnected) {
      final AuthPhoneData phoneData = await apiProvider.authPhone(phone: phone,);
      return phoneData;
    }
  }

  @override
  Future fetchData({String phone, int code}) async {
    if(await networkInfo.isConnected) {
      final VerificationSMSData smsData = await apiProvider.verifySms(phone: phone, code: code);
      final UserData userData = await apiProvider.getUser();
      authDBProvider.storeUser(
        token: smsData.token,
        id: userData.id,
        fullName: userData.fullName,
      );

      print("TOKEN___${json.decode(authDBProvider.toString())}");
      return smsData;
    }
  }
}