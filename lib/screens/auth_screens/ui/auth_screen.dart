import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/screens/auth_screens/bloc/auth_bloc.dart';
import 'package:rosseti/screens/auth_screens/injections/auth_repository_di.dart';
import 'package:rosseti/screens/auth_screens/repository/auth_repository.dart';

import 'auth_screen_view.dart';

class AuthScreen extends StatelessWidget {
  String phone;

  AuthScreen({this.phone});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBlocImpl>(
      create: (context) => AuthBlocImpl(
        authRepository: AuthRepositoryInject.authRepository(),
        authenticationManager: locator.get<AuthenticationManager>(),
      ),
      child: AuthScreenView(),
    );
  }
}
