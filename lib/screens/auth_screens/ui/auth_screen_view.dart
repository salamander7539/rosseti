import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/auth_screens/bloc/auth_bloc.dart';
import 'package:rosseti/screens/auth_screens/ui/verify_sms.dart';
import 'package:rosseti/screens/auth_screens/ui/verify_sms_view.dart';

class AuthScreenView extends StatefulWidget {
  const AuthScreenView({Key key}) : super(key: key);

  @override
  _AuthScreenViewState createState() => _AuthScreenViewState();
}

class _AuthScreenViewState extends State<AuthScreenView> {
  var controller;
  bool check;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    check = false;
    controller = new MaskedTextController(mask: '+7 000 000-00-00');
    controller.afterChange = (String previous, String next) {
      if (next.length > previous.length) {
        controller.selection = TextSelection.fromPosition(
            TextPosition(offset: controller.text.length));
      }
      return false;
    };
    controller.beforeChange = (String previous, String next) {
      if (controller.text == '8') {
        controller.updateText('+7 ');
      }
      return true;
    };
    controller.text = '+7 ';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Transform(
        transform: Matrix4.translationValues(0, -80, 0),
        child: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .87,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(24.0),
                ),
                child: TextFormField(
                  controller: controller,
                  autofocus: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    hintText: "Телефон",
                  ),
                  style:
                      GoogleFonts.aBeeZee(textStyle: TextStyle(fontSize: 20.0)),
                  onChanged: (value) {
                    if (check == true) {
                      setState(() {
                        check = false;
                      });
                    }
                    controller.text = value;
                    print(controller.text.length);
                    print(controller.text);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    if (controller.text.length == 16) {
                      controller.text = controller.text.replaceAll('-', '');
                      controller.text = controller.text.replaceAll(' ', '');
                      BlocProvider.of<AuthBlocImpl>(context)
                          .add(SendPhone(phone: controller.text));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VerifyScreen(
                                    phone: controller.text,
                                  )));
                    } else {
                      setState(() {
                        check = true;
                      });
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(24.0)),
                    ),
                    shadowColor: Colors.grey.withOpacity(0.8),
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width * .8,
                    height: 58.0,
                    child: Center(
                      child: Text(
                        'Далее',
                        style: GoogleFonts.aBeeZee(
                            fontSize: 20.0, color: AppColor.mainColor),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Visibility(
                  visible: check,
                  child: Text(
                    'Введите правильный номер',
                    style: GoogleFonts.aBeeZee(
                        textStyle: TextStyle(color: Colors.red)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
