import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/screens/auth_screens/bloc/auth_bloc.dart';
import 'package:rosseti/screens/auth_screens/injections/auth_repository_di.dart';
import 'package:rosseti/screens/auth_screens/ui/verify_sms_view.dart';

class VerifyScreen extends StatelessWidget {
  final String phone;

  const VerifyScreen({Key key, this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBlocImpl(
        authRepository: AuthRepositoryInject.authRepository(),
        authenticationManager: locator.get<AuthenticationManager>(),
      ),
      child: VerifyScreenView(phone: phone,),
    );
  }
}
