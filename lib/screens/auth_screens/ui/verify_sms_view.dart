import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/auth_screens/bloc/auth_bloc.dart';
import 'package:rosseti/screens/home_screen/ui/home_screen.dart';

class VerifyScreenView extends StatefulWidget {
  final String phone;

  const VerifyScreenView({Key key, this.phone}) : super(key: key);

  @override
  _VerifyScreenViewState createState() => _VerifyScreenViewState(phone: phone);
}

class _VerifyScreenViewState extends State<VerifyScreenView> {
  final String phone;
  TextEditingController codeController;

  _VerifyScreenViewState({this.phone});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    codeController = new TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Transform(
        transform: Matrix4.translationValues(0, -80, 0),
        child: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .87,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(24.0),
                ),
                child: TextFormField(
                  controller: codeController,
                  autofocus: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    hintText: "Код из СМС",
                  ),
                  onChanged: (value) {
                    codeController.text = value;
                    codeController.selection = TextSelection.fromPosition(TextPosition(offset: codeController.text.length));
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    BlocProvider.of<AuthBlocImpl>(context).add(SendSMS(phone: phone, code: int.parse(codeController.text)));
                    Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen(),));
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(24.0)),
                    ),
                    shadowColor: Colors.grey.withOpacity(0.8),
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width * .8,
                    height: 58.0,
                    child: Center(
                      child: Text('Готово', style: GoogleFonts.aBeeZee(fontSize: 20.0, color: AppColor.mainColor),),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
