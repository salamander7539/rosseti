import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/auth_screens/ui/auth_screen.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Image.asset(
                  'assets/images/rosseti_logo.png',
                  height: 200.0,
                ),
                Text(
                  'seti.inno',
                  style: GoogleFonts.aBeeZee(
                    textStyle: TextStyle(color: AppColor.mainColor),
                    fontSize: 62.0,
                  ),
                ),
                Text(
                  'Рационализатор',
                  style: GoogleFonts.aBeeZee(fontSize: 20.0),
                )
              ],
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(24.0)),
                ),
                shadowColor: Colors.grey.withOpacity(0.8),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AuthScreen(),
                  ),
                );
              },
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: 58.0,
                child: Center(
                  child: Text('Регистрация', style: GoogleFonts.aBeeZee(fontSize: 20.0, color: AppColor.mainColor),),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
