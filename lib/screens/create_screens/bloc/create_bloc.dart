import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/create_screens/injections/create_api_provider_di.dart';
import 'package:rosseti/screens/create_screens/models/answer_request_model.dart';
import 'package:rosseti/screens/create_screens/models/topics.dart';
import 'package:rosseti/screens/create_screens/repository/create_repository.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rxdart/rxdart.dart';

part 'create_event.dart';

part 'create_state.dart';

abstract class CreateBloc extends Bloc<CreateEvent, CreateState> {
  CreateBloc(CreateState initialState) : super(initialState);
}

class CreateBlocImpl extends Bloc<CreateEvent, CreateState> {
  final CreateRequestRepository createRequestRepository;

  CreateBlocImpl({@required this.createRequestRepository})
      : assert(createRequestRepository != null),
        super(CreateInitial());

  List<Topic> topicList = [];

  @override
  Stream<CreateState> mapEventToState(CreateEvent event) async* {
    if (event is SendData) {
      try {
        final RequestData requestData = await createRequestRepository
            .sendRequest(
          title: event.title,
          topicId: event.topicId,
          existingSolutionText: event.proposedSolutionText,
          existingSolutionImage: event.existingSolutionImage,
          existingSolutionVideo: event.proposedSolutionVideo,
          proposedSolutionText: event.proposedSolutionText,
          proposedSolutionImage: event.existingSolutionImage,
          proposedSolutionVideo: event.proposedSolutionVideo,
          positiveEffect: event.positiveEffect,
        );
        yield SuccessCreateState(requestData: requestData);
      } on Exception catch (e) {
        yield ErrorCreateState(error: e.toString());
      }
    }
  }
}

class DropdownBloc {
  final _repository = CreateRequestRepositoryImpl(
      networkInfo: locator.get<NetworkInfo>(),
      createApiProvider: CreateApiProviderInject.createApiProvider());

  final _selectedTopic = BehaviorSubject<Topic>();


  Future<List<Topic>> topics;

  Stream<Topic> get selectedTopic => _selectedTopic;

  void selectedCityEvent(Topic topic) {
    _selectedTopic.add(topic);
    print(_selectedTopic.value.id);
  }

  DropdownBloc() {
    topics = _repository.topicData();
    // topics = _selectedTopic$.switchMap((d) =>
    //     Stream.fromFuture(_repository.topicData()).startWith(null))
    //   ..listen((e) => _selectedTopic$.add(null));
  }

  void dispose() {
    _selectedTopic.close();
  }
}
