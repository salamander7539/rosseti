part of 'create_bloc.dart';

abstract class CreateEvent extends Equatable {
  const CreateEvent();
}

class GetTopics extends CreateEvent {
  @override
  List<Object> get props => [];
}

class SendData extends CreateEvent {
  final String title;
  final int topicId;
  final String existingSolutionText;
  final String existingSolutionImage;
  final String existingSolutionVideo;
  final String proposedSolutionText;
  final String proposedSolutionImage;
  final String proposedSolutionVideo;
  final String positiveEffect;

  SendData(
      {this.title,
      this.topicId,
      this.existingSolutionText,
      this.existingSolutionImage,
      this.existingSolutionVideo,
      this.proposedSolutionText,
      this.proposedSolutionImage,
      this.proposedSolutionVideo,
      this.positiveEffect});

  @override
  List<Object> get props => [
        existingSolutionText,
        existingSolutionImage,
        existingSolutionVideo,
        proposedSolutionText,
        proposedSolutionImage,
        proposedSolutionVideo,
        positiveEffect,
        topicId,
        title
      ];
}
