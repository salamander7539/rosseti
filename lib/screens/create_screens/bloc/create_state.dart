part of 'create_bloc.dart';

abstract class CreateState extends Equatable {
  const CreateState();
}

class CreateInitial extends CreateState {
  @override
  List<Object> get props => [];
}

class SuccessCreateState extends CreateState {
  final RequestData requestData;

  SuccessCreateState({this.requestData});

  @override
  List<Object> get props => [requestData];
}

class TopicsLoadedState extends CreateState {
  final List<Topic> topicList;

  TopicsLoadedState({this.topicList});

  @override
  // TODO: implement props
  List<Object> get props => [topicList];
}

class ErrorCreateState extends CreateState {
  final String error;

  ErrorCreateState({this.error});

  @override
  List<Object> get props => [error];
}