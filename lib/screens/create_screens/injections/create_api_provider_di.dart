
import 'package:rosseti/screens/create_screens/provider/create_api_provider.dart';
import 'package:rosseti/screens/projects_screens/providers/all_projects_api_provider.dart';

class CreateApiProviderInject {
  static CreateApiProvider _createApiProvider;

  CreateApiProviderInject._();

  static CreateApiProvider createApiProvider() {
    if (_createApiProvider == null) {
      _createApiProvider = CreateApiProviderImpl();
    }
    return _createApiProvider;
  }
}