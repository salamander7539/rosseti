import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/create_screens/injections/create_api_provider_di.dart';
import 'package:rosseti/screens/create_screens/repository/create_repository.dart';
import 'package:rosseti/screens/projects_screens/injections/projects_api_provider_di.dart';
import 'package:rosseti/screens/projects_screens/repositories/projects_repository.dart';

class CreateRepositoryInject {
  static CreateRequestRepository _createRequestRepository;

  CreateRepositoryInject._();

  static CreateRequestRepository createRequestRepository() {
    if (_createRequestRepository == null) {
      _createRequestRepository = CreateRequestRepositoryImpl(
        createApiProvider: CreateApiProviderInject.createApiProvider(),
        networkInfo: locator.get<NetworkInfo>(),
      );
    }
    return _createRequestRepository;
  }
}