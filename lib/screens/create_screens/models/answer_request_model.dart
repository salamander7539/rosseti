class RequestData {
  RequestData({
    this.success,
  });

  bool success;

  factory RequestData.fromJson(Map<String, dynamic> json) => RequestData(
    success: json["success"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
  };
}
