// To parse this JSON data, do
//
//     final topicData = topicDataFromJson(jsonString);

import 'dart:convert';

TopicData topicDataFromJson(String str) => TopicData.fromJson(json.decode(str));

String topicDataToJson(TopicData data) => json.encode(data.toJson());

class TopicData {
  TopicData({
    this.success,
    this.topics,
  });

  bool success;
  List<Topic> topics;

  factory TopicData.fromJson(Map<String, dynamic> json) => TopicData(
    success: json["success"],
    topics: List<Topic>.from(json["topics"].map((x) => Topic.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "topics": List<dynamic>.from(topics.map((x) => x.toJson())),
  };
}

class Topic {
  Topic({
    this.id,
    this.title,
  });

  int id;
  String title;

  factory Topic.fromJson(Map<String, dynamic> json) => Topic(
    id: json["id"],
    title: json["title"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
  };
}
