import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rosseti/core/global/api.dart';
import 'package:rosseti/core/global/bearer.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/models/user_model.dart';
import 'package:rosseti/screens/create_screens/models/answer_request_model.dart';
import 'package:rosseti/screens/create_screens/models/topics.dart';

abstract class CreateApiProvider<T> {
  Future<T> sendRequest({
    String title,
    int topicId,
    String existingSolutionText,
    String existingSolutionImage,
    String existingSolutionVideo,
    String proposedSolutionText,
    String proposedSolutionImage,
    String proposedSolutionVideo,
    String positiveEffect,

  });

  Future<T> getTopics();
}

class CreateApiProviderImpl extends CreateApiProvider {
  final Dio _dio = Dio();

  @override
  Future sendRequest(
      {String title,
      int topicId,
      String existingSolutionText,
      String existingSolutionImage,
      String existingSolutionVideo,
      String proposedSolutionText,
      String proposedSolutionImage,
      String proposedSolutionVideo,

      String positiveEffect}) async {
    try {
      String bearer = await getBearerToken();
      var secureStorage = locator.get<SecureStorage>();
      UserData userData = UserData.fromJson(
          json.decode(await secureStorage.getData('UserData')));
      FormData formData = FormData.fromMap({
        'title': title,
        'topic_id': topicId,
        'existing_solution_text': existingSolutionText,
        'existing_solution_image': existingSolutionImage != null ? await MultipartFile.fromFile(existingSolutionImage, filename:existingSolutionImage.split('/').last) : null,
        'existing_solution_video': existingSolutionVideo != null ? await MultipartFile.fromFile(existingSolutionVideo, filename:existingSolutionVideo.split('/').last) : null,
        'proposed_solution_text': proposedSolutionText,
        'proposed_solution_image':  proposedSolutionImage != null ? await MultipartFile.fromFile(proposedSolutionImage, filename:proposedSolutionImage.split('/').last) : null,
        'proposed_solution_video': proposedSolutionVideo != null ? await MultipartFile.fromFile(proposedSolutionVideo, filename:proposedSolutionVideo.split('/').last) : null,
        'positive_effect': positiveEffect,
        'name': userData.fullName
      });

      Response response = await _dio.post(
        "$apiUrl/suggestions/store",
        data: formData,
        options: Options(
          headers: {'Authorization': 'Bearer $bearer'},
        ),
      );
      if (response.statusCode == 200) {
        return RequestData.fromJson(response.data);
      }
    } on Exception catch (e) {
      print('create answer ${e.toString()}');
    }
  }

  @override
  Future getTopics() async {
    try {
      String bearer = await getBearerToken();

      Response response = await _dio.get(
        '$apiUrl/topics',
        options: Options(
          headers: {'Authorization': 'Bearer $bearer'},
        ),
      );
      if (response.statusCode == 200) {
        var jsonResponse = response.data['topics'];
        print(jsonResponse.runtimeType);
        List<Topic> topicList = [];
        jsonResponse
            .map((topics) => topicList.add(Topic.fromJson(topics)))
            .toList();
        return topicList.length == 0 ? null : topicList;
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }
}
