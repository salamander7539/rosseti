import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/create_screens/models/answer_request_model.dart';
import 'package:rosseti/screens/create_screens/models/topics.dart';
import 'package:rosseti/screens/create_screens/provider/create_api_provider.dart';

abstract class CreateRequestRepository<T> {
  Future<T> sendRequest({
    String title,
    int topicId,
    String existingSolutionText,
    String existingSolutionImage,
    String existingSolutionVideo,
    String proposedSolutionText,
    String proposedSolutionImage,
    String proposedSolutionVideo,
    String positiveEffect,

  });
  Future<List<Topic>> topicData();
}

class CreateRequestRepositoryImpl extends CreateRequestRepository {
  final NetworkInfo networkInfo;
  final CreateApiProvider createApiProvider;

  CreateRequestRepositoryImpl(
      {@required this.networkInfo, @required this.createApiProvider})
      : assert(createApiProvider != null),
        assert(networkInfo != null);


  @override
  Future sendRequest(
      {String existingSolutionText,
        String existingSolutionImage,
      String existingSolutionVideo,
      String proposedSolutionText,
        String proposedSolutionImage,
      String proposedSolutionVideo,
      String positiveEffect,
      String title,

      int topicId}) async {
    if (await networkInfo.isConnected) {
      final RequestData requestData = await createApiProvider.sendRequest(
        title: title,
        topicId: topicId,
        existingSolutionText: existingSolutionText,
        existingSolutionImage: existingSolutionImage,
        existingSolutionVideo: existingSolutionVideo,
        proposedSolutionText: proposedSolutionText,
        proposedSolutionImage: proposedSolutionImage,
        proposedSolutionVideo: proposedSolutionVideo,
        positiveEffect: positiveEffect,

      );
      return requestData;
    }
  }

  @override
  Future<List<Topic>> topicData() async {
    if(await networkInfo.isConnected) {
      final List<Topic> topicData = await createApiProvider.getTopics();
      return topicData;
    }
  }
}
