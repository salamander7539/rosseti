import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/screens/create_screens/bloc/create_bloc.dart';
import 'package:rosseti/screens/create_screens/injections/create_repository_di.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';

class CreateScreen extends StatelessWidget {
  const CreateScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateBlocImpl>(
      create: (context) => CreateBlocImpl(
          createRequestRepository: CreateRepositoryInject.createRequestRepository(),
      ),
      child: CreateScreenView(),
    );
  }
}
