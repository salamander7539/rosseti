import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/screens/create_screens/bloc/create_bloc.dart';
import 'package:rosseti/screens/create_screens/injections/create_repository_di.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view_1.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view_2.dart';

class CreateScreenSecond extends StatelessWidget {

  final int topicId;
  final String image1;
  final String title;
  final String text1;

  const CreateScreenSecond({Key key, this.topicId, this.image1, this.title, this.text1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateBlocImpl>(
      create: (context) => CreateBlocImpl(
        createRequestRepository: CreateRepositoryInject.createRequestRepository(),
      ),
      child: CreateScreenViewSecond(title: title, text1: text1, image1: image1, topicId: topicId,),
    );
  }
}