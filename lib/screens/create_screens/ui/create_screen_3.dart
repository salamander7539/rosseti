import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/screens/create_screens/bloc/create_bloc.dart';
import 'package:rosseti/screens/create_screens/injections/create_repository_di.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view_1.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view_3.dart';

class CreateScreenThird extends StatelessWidget {

  final int topicId;
  final String image1;
  final String title;
  final String text1;
  final String image2;
  final String text2;

  const CreateScreenThird({Key key, this.topicId, this.image1, this.title, this.text1, this.image2, this.text2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CreateBlocImpl>(
      create: (context) => CreateBlocImpl(
        createRequestRepository: CreateRepositoryInject.createRequestRepository(),
      ),
      child: CreateScreenViewThird(title: title, image1: image1, text1: text1, image2: image2, text2: text2, topicId: topicId,),
    );
  }
}