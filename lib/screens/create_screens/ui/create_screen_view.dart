import 'dart:convert';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/api.dart';
import 'package:rosseti/core/global/bearer.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:http/http.dart' as http;
import 'package:rosseti/screens/create_screens/bloc/create_bloc.dart';
import 'package:rosseti/screens/create_screens/models/topics.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_1.dart';


class CreateScreenView extends StatefulWidget {
  const CreateScreenView({Key key}) : super(key: key);

  @override
  _CreateScreenViewState createState() => _CreateScreenViewState();
}

class _CreateScreenViewState extends State<CreateScreenView> {
  TextEditingController _controller;
  CreateBlocImpl createBlocImpl;
  final Dio _dio = Dio();
  TextEditingController _suggestionsController;
  AutoCompleteTextField searchTextField;
  GlobalKey<AutoCompleteTextFieldState<Topic>> key = new GlobalKey();

  List<Topic> topic = [];
  String dropDownValue;
  DropdownBloc _dropdownBloc;
  int chosenId;

  @override
  void initState() {
    // TODO: implement initState
    createBlocImpl = BlocProvider.of<CreateBlocImpl>(context);
    _dropdownBloc = DropdownBloc();
    createBlocImpl.add(GetTopics());
    // getT();
    super.initState();
    _controller = new TextEditingController();
    _suggestionsController = new TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
    _suggestionsController.dispose();
  }

  // getT() async {
  //   topic = await getTopics();
  //   return topic;
  // }

  // Future<List<Topic>> getTopics() async {
  //   try {
  //     String bearer = await getBearerToken();
  //     var response = await http.get(
  //       Uri.parse('$apiUrl/topics'),
  //       headers: {'Authorization': 'Bearer $bearer'},
  //     );
  //     if (response.statusCode == 200) {
  //       var jsonResponse = response.body;
  //       List topic = json.decode(response.body)['topics'];
  //       return topic.map<Topic>((topics) => Topic.fromJson(topics)).toList();
  //     }
  //   } on Exception catch (e) {
  //     print(e.toString());
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 68.0, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: AppColor.mainColor,
                        size: 28,
                      ),
                    ),
                    Text(
                      'Создать',
                      style: GoogleFonts.aBeeZee(
                        textStyle: TextStyle(
                            fontSize: 36.0, color: AppColor.mainColor),
                      ),
                    ),
                    Image.asset(
                      'assets/images/rosseti_logo.png',
                      height: 50.0,
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  'Расскажите о предложении',
                  style: GoogleFonts.aBeeZee(
                    textStyle:
                        TextStyle(fontSize: 20.0, color: AppColor.mainColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 34.0),
                child: Text(
                  'Виберите тему и название ',
                  style: GoogleFonts.aBeeZee(
                    textStyle:
                        TextStyle(fontSize: 20.0, color: AppColor.mainColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 34.0),
                child: Container(
                  height: 60.0,
                  width: MediaQuery.of(context).size.width * .87,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.0),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(24.0),
                    border: Border.all(color: AppColor.mainColor, width: 2.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FutureBuilder<List<Topic>>(
                        future: _dropdownBloc.topics,
                        builder: (context, snapshot) {
                          return StreamBuilder<Topic>(
                              stream: _dropdownBloc.selectedTopic,
                              builder: (context, items) {
                                return DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                      icon: SvgPicture.asset(
                                          'assets/svg_images/vector.svg'),
                                      value: items.data,
                                      onChanged: (Topic newTopic) {
                                        _dropdownBloc
                                            .selectedCityEvent(newTopic);
                                        chosenId = newTopic.id;
                                      },
                                      hint: Padding(
                                        padding: const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "Тема проекта",
                                          style: GoogleFonts.aBeeZee(textStyle: TextStyle(fontSize: 18.0)),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                      items: snapshot?.data
                                          ?.map<DropdownMenuItem<Topic>>(
                                              (data) => DropdownMenuItem(
                                                    value: data,
                                                    child: SizedBox(
                                                      width: MediaQuery.of(context).size.width * .7,
                                                      child: Text(
                                                        data.title,
                                                        style: GoogleFonts.aBeeZee(textStyle: TextStyle(color: AppColor.mainColor, fontSize: 20.0)),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                  ))
                                          ?.toList()),

                                  // items: topic.map((Topic data) {
                                  //   return DropdownMenuItem(
                                  //     value: data.id.toString(),
                                  //     child: SizedBox(
                                  //       width: 300.0,
                                  //       child: Text(
                                  //         data.title,
                                  //         overflow: TextOverflow.ellipsis,
                                  //       ),
                                  //     ),
                                  //   );
                                  // }).toList(),
                                );
                              });
                        }),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 34.0),
                child: Container(
                  height: 60,
                  width: MediaQuery.of(context).size.width * .87,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.0),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(24.0),
                    border: Border.all(color: AppColor.mainColor, width: 2.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      controller: _controller,
                      autofocus: true,
                      style: GoogleFonts.aBeeZee(textStyle: TextStyle(color: AppColor.mainColor, fontSize: 18.0)),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Название",
                        hintStyle: GoogleFonts.aBeeZee(textStyle: TextStyle(fontSize: 18.0)),
                      ),
                      onChanged: (value) {
                        _controller.text = value;
                        _controller.selection = TextSelection.fromPosition(
                            TextPosition(offset: _controller.text.length));
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 140.0),
              child: ElevatedButton(
                onPressed: () {
                  if (_controller.text != null) {
                    print(_controller.text);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CreateScreenFirst(
                          title: _controller.text,
                          topicId: chosenId,
                        ),
                      ),
                    );
                  }
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(24.0)),
                  ),
                  shadowColor: Colors.grey.withOpacity(0.8),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width * .8,
                  height: 58.0,
                  child: Center(
                    child: Text(
                      'Готово',
                      style: GoogleFonts.aBeeZee(
                          fontSize: 20.0, color: AppColor.mainColor),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
