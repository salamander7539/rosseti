import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/create_screens/models/media_source.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_2.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rosseti/screens/create_screens/widgets/chewie.dart';
import 'package:video_player/video_player.dart';

class CreateScreenViewFirst extends StatefulWidget {
  final int topicId;
  final String title;

  const CreateScreenViewFirst({Key key, this.title, this.topicId})
      : super(key: key);

  @override
  _CreateScreenViewFirstState createState() =>
      _CreateScreenViewFirstState(title: title, topicId: topicId);
}

class _CreateScreenViewFirstState extends State<CreateScreenViewFirst> {
  final int topicId;
  final String title;
  VideoPlayerController _controller;

  _CreateScreenViewFirstState({this.topicId, this.title});

  File imageMedia;
  File videoMedia;

  TextEditingController _textEditingController;

  Future pickGalleryImage(BuildContext buildContext,
      {MediaSource source}) async {
    final MediaSource mediaSource = source;
    final getMedia = source == MediaSource.image
        ? ImagePicker().getImage
        : ImagePicker().getVideo;

    final media = await getMedia(source: ImageSource.gallery);
    final file = File(media.path);

    if (file == null) {
      return;
    } else {
      setState(() {
        imageMedia = file;
      });
    }
  }

  Future pickGalleryVideo(BuildContext buildContext,
      {MediaSource source}) async {
    final MediaSource mediaSource = source;
    final getMedia = source == MediaSource.image
        ? ImagePicker().getImage
        : ImagePicker().getVideo;

    final media = await getMedia(source: ImageSource.gallery);
    final file = File(media.path);

    if (file == null) {
      return;
    } else {
      print(file);
      setState(() {
        videoMedia = file;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingController = new TextEditingController();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 68.0, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: AppColor.mainColor,
                        size: 28,
                      ),
                    ),
                    Text(
                      'Создать',
                      style: GoogleFonts.aBeeZee(
                        textStyle: TextStyle(
                            fontSize: 36.0, color: AppColor.mainColor),
                      ),
                    ),
                    Image.asset(
                      'assets/images/rosseti_logo.png',
                      height: 50.0,
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  'Расскажите как сейчас',
                  style: GoogleFonts.aBeeZee(
                    textStyle:
                        TextStyle(fontSize: 20.0, color: AppColor.mainColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 34.0),
                child: Container(
                  width: MediaQuery.of(context).size.width * .87,
                  height: MediaQuery.of(context).size.height * .35,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.0),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(24.0),
                    border: Border.all(color: AppColor.mainColor, width: 2.0),
                  ),
                  child: TextFormField(
                    controller: _textEditingController,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    autofocus: true,
                    style: GoogleFonts.aBeeZee(textStyle: TextStyle(color: AppColor.mainColor, fontSize: 20.0)),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                    ),
                    onChanged: (value) {
                      _textEditingController.text = value;
                      _textEditingController.selection =
                          TextSelection.fromPosition(TextPosition(
                              offset: _textEditingController.text.length));
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  'Добавьте фото или видео',
                  style: GoogleFonts.aBeeZee(
                    textStyle:
                        TextStyle(fontSize: 20.0, color: AppColor.mainColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 26.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RawMaterialButton(
                        onPressed: () => pickGalleryVideo(context,
                            source: MediaSource.video),
                        elevation: 2.0,
                        fillColor: Colors.white,
                        child: SvgPicture.asset(
                          'assets/svg_images/video.svg',
                          color: Colors.black,
                        ),
                        padding: EdgeInsets.all(15.0),
                        shape: CircleBorder(),
                      ),
                      RawMaterialButton(
                        onPressed: () => pickGalleryImage(context,
                            source: MediaSource.image),
                        elevation: 2.0,
                        fillColor: Colors.white,
                        child: SvgPicture.asset(
                          'assets/svg_images/image.svg',
                          color: Colors.black,
                        ),
                        padding: EdgeInsets.all(15.0),
                        shape: CircleBorder(),
                      ),
                      imageMedia == null
                          ? Container()
                          : Container(
                              width: 120,
                              height: 60,
                              child: Image.file(imageMedia),
                            ),
                      videoMedia == null
                          ? Container()
                          : ChewieWidget(videoPlayerController: VideoPlayerController.file(videoMedia), looping: true,),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 50.0),
              child: ElevatedButton(
                onPressed: () async {

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CreateScreenSecond(
                                topicId: topicId,
                                title: title,
                                image1: imageMedia != null
                                    ? imageMedia.path
                                    : null,
                                text1: _textEditingController.text,
                              )));
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(24.0)),
                  ),
                  shadowColor: Colors.grey.withOpacity(0.8),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width * .8,
                  height: 58.0,
                  child: Center(
                    child: Text(
                      'Дальше',
                      style: GoogleFonts.aBeeZee(
                          fontSize: 20.0, color: AppColor.mainColor),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
