import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/create_screens/bloc/create_bloc.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rosseti/screens/home_screen/ui/home_screen.dart';

class CreateScreenViewThird extends StatefulWidget {

  final int topicId;
  final String image1;
  final String title;
  final String text1;
  final String image2;
  final String text2;

  const CreateScreenViewThird({Key key, this.topicId, this.image1, this.title, this.text1, this.image2, this.text2}) : super(key: key);

  @override
  _CreateScreenViewThirdState createState() => _CreateScreenViewThirdState(title: title, image1: image1, image2: image2, text1: text1, text2: text2, topicId: topicId);
}

class _CreateScreenViewThirdState extends State<CreateScreenViewThird> {

  final int topicId;
  final String image1;
  final String title;
  final String text1;
  final String image2;
  final String text2;

  _CreateScreenViewThirdState(
      {this.image1, this.title, this.topicId, this.text1, this.image2, this.text2});

  TextEditingController _textEditingController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingController = new TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 68.0, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: AppColor.mainColor,
                        size: 28,
                      ),
                    ),
                    Text(
                      'Создать',
                      style: GoogleFonts.aBeeZee(
                        textStyle:
                        TextStyle(fontSize: 36.0, color: AppColor.mainColor),
                      ),
                    ),
                    Image.asset('assets/images/rosseti_logo.png', height: 50.0,)
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  'Расскажите как будет',
                  style: GoogleFonts.aBeeZee(
                    textStyle:
                    TextStyle(fontSize: 20.0, color: AppColor.mainColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 34.0),
                child: Container(
                  width: MediaQuery.of(context).size.width * .87,
                  height: MediaQuery.of(context).size.height * .35,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.0),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(24.0),
                    border: Border.all(color: AppColor.mainColor,width: 2.0),
                  ),
                  child: TextFormField(
                    controller: _textEditingController,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    style: GoogleFonts.aBeeZee(textStyle: TextStyle(color: AppColor.mainColor, fontSize: 20.0)),
                    autofocus: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                    ),
                    onChanged: (value) {
                      _textEditingController.text = value;
                      _textEditingController.selection = TextSelection.fromPosition(TextPosition(offset: _textEditingController.text.length));
                    },
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 50.0),
              child: ElevatedButton(
                onPressed: () {

                  BlocProvider.of<CreateBlocImpl>(context)
                      .add(SendData(title: title, topicId: topicId, existingSolutionText: text1, proposedSolutionText: text2, existingSolutionImage: image1, proposedSolutionImage: image2, positiveEffect: _textEditingController.text,));
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(24.0)),
                  ),
                  shadowColor: Colors.grey.withOpacity(0.8),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width * .8,
                  height: 58.0,
                  child: Center(
                    child: Text('Готово', style: GoogleFonts.aBeeZee(fontSize: 20.0, color: AppColor.mainColor),),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
