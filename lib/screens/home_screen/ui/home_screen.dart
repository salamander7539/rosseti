import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen.dart';
import 'package:rosseti/screens/create_screens/ui/create_screen_view.dart';
import 'package:rosseti/screens/projects_screens/ui/projects_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Widget button({String text, String imagePath, Widget screen}) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(24.0)),
        ),
        shadowColor: Colors.grey.withOpacity(0.8),
      ),
      onPressed: () {
        if (screen != null) {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => screen));
        }
      },
      child: Container(
        height: 165,
        width: 305,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SvgPicture.asset(imagePath),
              Text(
                text,
                style: GoogleFonts.aBeeZee(
                  textStyle: TextStyle(
                    color: Color(0xFF205692),
                    fontSize: 20.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: 'seti.inno',
                  style: GoogleFonts.aBeeZee(
                    textStyle: TextStyle(
                      color: Color(0xFF205692),
                      fontSize: 36.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
                WidgetSpan(
                  child: Transform(
                    transform: Matrix4.translationValues(0, 20, 0),
                    child: Image.asset(
                      'assets/images/rosseti_logo.png',
                      height: 60,
                    ),
                  ),
                ),
              ]),
            ),
            button(text: 'Создать\nпредложение', imagePath: 'assets/svg_images/create.svg', screen: CreateScreen()),
            button(text: 'Заявки', imagePath: 'assets/svg_images/expertise.svg', screen: ProjectsScreen()),
            button(text: 'Экспертизы', imagePath: 'assets/svg_images/requests.svg'),
          ],
        ),
      ),
    );
  }
}
