import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rosseti/screens/projects_screens/models/all_request_model.dart';
import 'package:rosseti/screens/projects_screens/repositories/projects_repository.dart';

part 'projects_event.dart';

part 'projects_state.dart';

abstract class ProjectsBloc extends Bloc<ProjectsEvent, ProjectsState> {
  ProjectsBloc(ProjectsState initialState) : super(initialState);

}

class ProjectsBlocImpl extends Bloc<ProjectsEvent, ProjectsState> {
  final ProjectsRepository projectsRepository;

  ProjectsBlocImpl({this.projectsRepository}) : super(ProjectsInitial());
  List<Suggestion> suggestionList = [];
  bool isLoading = false;

  @override
  Stream<ProjectsState> mapEventToState(ProjectsEvent event) async* {
    if (event is FetchProjects) {
      if(!isLoading) {
        isLoading = true;
        try {
          suggestionList = await projectsRepository.projectsData();
          yield ProjectsLoaded(suggestionList: suggestionList);
        } on Exception catch (e) {
          yield ProjectsErrorState(error: 'Failed to fetch Suggestions');
        }
        isLoading = true;
      }
    }
    if (event is RatingSendEvent) {
      try {
        await projectsRepository.rating(suggestionId: event.suggestionId, value: event.value);
      } on Exception catch (e) {
        yield ProjectsErrorState(error: 'Failed to fetch SuggestionsID');
      }
    }
  }
}
