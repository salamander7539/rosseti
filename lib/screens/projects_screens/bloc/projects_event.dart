part of 'projects_bloc.dart';

abstract class ProjectsEvent extends Equatable {
  const ProjectsEvent();
}

class FetchProjects extends ProjectsEvent {
  @override
  List<Object> get props => [];
}

class RatingSendEvent extends ProjectsEvent {
  final int suggestionId;
  final int value;

  RatingSendEvent({this.suggestionId, this.value});

  @override
  // TODO: implement props
  List<Object> get props => [suggestionId, value];
}
