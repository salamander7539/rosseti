part of 'projects_bloc.dart';

abstract class ProjectsState extends Equatable {
  const ProjectsState();
}

class ProjectsInitial extends ProjectsState {
  @override
  List<Object> get props => [];
}

class ProjectsLoaded extends ProjectsState {

  final List<Suggestion> suggestionList;

  ProjectsLoaded({this.suggestionList});

  @override
  List<Object> get props => [this.suggestionList];
}


class ProjectsErrorState extends ProjectsState {
  final String error;

  const ProjectsErrorState({this.error});

  @override
  List<Object> get props => [error];
}
