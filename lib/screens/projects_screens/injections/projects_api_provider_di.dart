
import 'package:rosseti/screens/projects_screens/providers/all_projects_api_provider.dart';

class ProjectsApiProviderInject {
  static AllProjectsProvider _allProjectsProvider;

  ProjectsApiProviderInject._();

  static AllProjectsProvider allProjectsProvider() {
    if (_allProjectsProvider == null) {
      _allProjectsProvider = AllProjectsProviderImpl();
    }
    return _allProjectsProvider;
  }
}