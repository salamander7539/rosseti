import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/projects_screens/injections/projects_api_provider_di.dart';
import 'package:rosseti/screens/projects_screens/repositories/projects_repository.dart';

class ProjectsRepositoryInject {
  static ProjectsRepository _projectsRepository;

  ProjectsRepositoryInject._();

  static ProjectsRepository projectsRepository() {
    if (_projectsRepository == null) {
      _projectsRepository = ProjectsRepositoryImpl(
          allProjectsProvider: ProjectsApiProviderInject.allProjectsProvider(),
          networkInfo: locator.get<NetworkInfo>(),
      );
    }
    return _projectsRepository;
  }
}
