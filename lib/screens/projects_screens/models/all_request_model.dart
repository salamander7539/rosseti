class AllProjectsData {
  AllProjectsData({
    this.success,
    this.suggestions,
  });

  bool success;
  List<Suggestion> suggestions;

  factory AllProjectsData.fromJson(Map<String, dynamic> json) => AllProjectsData(
    success: json["success"],
    suggestions: List<Suggestion>.from(json["suggestions"].map((x) => Suggestion.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "suggestions": List<dynamic>.from(suggestions.map((x) => x.toJson())),
  };
}

class Suggestion {
  Suggestion({
    this.id,
    this.authorId,
    this.title,
    this.topicId,
    this.existingSolutionText,
    this.existingSolutionImage,
    this.existingSolutionVideo,
    this.proposedSolutionText,
    this.proposedSolutionImage,
    this.proposedSolutionVideo,
    this.positiveEffect,
    this.statusId,
    this.registrationNumber,
    this.rating,
    this.experted,
    this.author,
    this.comments,
  });

  int id;
  int authorId;
  String title;
  int topicId;
  String existingSolutionText;
  String existingSolutionImage;
  String existingSolutionVideo;
  String proposedSolutionText;
  String proposedSolutionImage;
  String proposedSolutionVideo;
  String positiveEffect;
  int statusId;
  dynamic registrationNumber;
  int rating;
  int experted;
  Author author;
  List<Comment> comments;

  factory Suggestion.fromJson(Map<String, dynamic> json) => Suggestion(
    id: json["id"],
    authorId: json["author_id"],
    title: json["title"],
    topicId: json["topic_id"],
    existingSolutionText: json["existing_solution_text"],
    existingSolutionImage: json["existing_solution_image"] == null ? null : json["existing_solution_image"],
    existingSolutionVideo: json["existing_solution_video"] == null ? null : json["existing_solution_video"],
    proposedSolutionText: json["proposed_solution_text"],
    proposedSolutionImage: json["proposed_solution_image"] == null ? null : json["proposed_solution_image"],
    proposedSolutionVideo: json["proposed_solution_video"] == null ? null : json["proposed_solution_video"],
    positiveEffect: json["positive_effect"],
    statusId: json["status_id"],
    registrationNumber: json["registration_number"],
    rating: json["rating"],
    experted: json["experted"],
    author: Author.fromJson(json["author"]),
    comments: List<Comment>.from(json["comments"].map((x) => Comment.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "author_id": authorId,
    "title": title,
    "topic_id": topicId,
    "existing_solution_text": existingSolutionText,
    "existing_solution_image": existingSolutionImage == null ? null : existingSolutionImage,
    "existing_solution_video": existingSolutionVideo == null ? null : existingSolutionVideo,
    "proposed_solution_text": proposedSolutionText,
    "proposed_solution_image": proposedSolutionImage == null ? null : proposedSolutionImage,
    "proposed_solution_video": proposedSolutionVideo == null ? null : proposedSolutionVideo,
    "positive_effect": positiveEffect,
    "status_id": statusId,
    "registration_number": registrationNumber,
    "rating": rating,
    "experted": experted,
    "author": author.toJson(),
    "comments": List<dynamic>.from(comments.map((x) => x.toJson())),
  };
}

class Author {
  Author({
    this.id,
    this.fullName,
    this.phone,
    this.topicId,
    this.email,
    this.commentsCount,
    this.ratingsCount,
    this.acceptedProposalsCount,
    this.deniedProposalsCount,
    this.proposalsCount,
  });

  int id;
  String fullName;
  String phone;
  dynamic topicId;
  dynamic email;
  int commentsCount;
  int ratingsCount;
  int acceptedProposalsCount;
  int deniedProposalsCount;
  int proposalsCount;

  factory Author.fromJson(Map<String, dynamic> json) => Author(
    id: json["id"],
    fullName: json["full_name"],
    phone: json["phone"],
    topicId: json["topic_id"],
    email: json["email"],
    commentsCount: json["comments_count"],
    ratingsCount: json["ratings_count"],
    acceptedProposalsCount: json["accepted_proposals_count"],
    deniedProposalsCount: json["denied_proposals_count"],
    proposalsCount: json["proposals_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "full_name": fullName,
    "phone": phone,
    "topic_id": topicId,
    "email": email,
    "comments_count": commentsCount,
    "ratings_count": ratingsCount,
    "accepted_proposals_count": acceptedProposalsCount,
    "denied_proposals_count": deniedProposalsCount,
    "proposals_count": proposalsCount,
  };
}

class Comment {
  Comment({
    this.id,
    this.text,
    this.userId,
    this.suggestionId,
    this.datetime,
    this.you,
    this.user,
  });

  int id;
  String text;
  int userId;
  int suggestionId;
  DateTime datetime;
  int you;
  Author user;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
    id: json["id"],
    text: json["text"],
    userId: json["user_id"],
    suggestionId: json["suggestion_id"],
    datetime: DateTime.parse(json["datetime"]),
    you: json["you"],
    user: Author.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "text": text,
    "user_id": userId,
    "suggestion_id": suggestionId,
    "datetime": datetime.toIso8601String(),
    "you": you,
    "user": user.toJson(),
  };
}
