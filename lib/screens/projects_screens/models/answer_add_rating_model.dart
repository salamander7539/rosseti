class AddRatingData {
  AddRatingData({
    this.success,
  });

  bool success;

  factory AddRatingData.fromJson(Map<String, dynamic> json) => AddRatingData(
    success: json["success"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
  };
}
