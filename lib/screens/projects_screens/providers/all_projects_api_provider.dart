import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:rosseti/core/global/api.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/secure_storage.dart';
import 'package:rosseti/screens/auth_screens/models/verification_sms_model.dart';
import 'package:rosseti/screens/projects_screens/models/all_request_model.dart';

abstract class AllProjectsProvider<T> {
  Future <List<Suggestion>> getAllProjects();
  Future<T> putRating({int suggestionId, int value});
}

class AllProjectsProviderImpl implements AllProjectsProvider {

  @override
  Future<List<Suggestion>> getAllProjects() async {
    SecureStorage secureStorage = locator.get<SecureStorage>();

    final rawUser = await secureStorage.getData('MainUser');

    final VerificationSMSData smsData = VerificationSMSData.fromJson(json.decode(rawUser));

    final String bearerToken = smsData.token;

    final Dio _dio = Dio();

    try {
      Response response = await _dio.get('$apiUrl/suggestions/index', options: Options(headers: {
        'Authorization': 'Bearer $bearerToken'}));
      if (response.statusCode == 200) {
        var jsonResponse = response.data['suggestions'];
        print(jsonResponse.runtimeType);
        List<Suggestion> suggestionList = [];
        jsonResponse.map((suggestion) => suggestionList.add(Suggestion.fromJson(suggestion))).toList();
        return suggestionList.length == 0 ? null : suggestionList;
      }
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<void> putRating({int suggestionId, int value}) async {
    SecureStorage secureStorage = locator.get<SecureStorage>();

    final rawUser = await secureStorage.getData('MainUser');

    final VerificationSMSData smsData = VerificationSMSData.fromJson(json.decode(rawUser));

    final String bearerToken = smsData.token;

    final Dio _dio = Dio();

    try {
      Response response = await _dio.post('$apiUrl/suggestions/rating/store', data: {
        'suggestion_id': suggestionId,
        'value': value,
      }, options: Options(headers: {
        'Authorization': 'Bearer $bearerToken'}));
      if (response.statusCode == 200) {
        print('rating posted');
      }
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

}