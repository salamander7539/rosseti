import 'package:flutter/cupertino.dart';
import 'package:rosseti/core/services/network_info.dart';
import 'package:rosseti/screens/projects_screens/models/all_request_model.dart';
import 'package:rosseti/screens/projects_screens/providers/all_projects_api_provider.dart';

abstract class ProjectsRepository<T> {
  Future<List<Suggestion>> projectsData();
  Future<T> rating({int suggestionId, int value});
}

class ProjectsRepositoryImpl implements ProjectsRepository {

  final AllProjectsProvider allProjectsProvider;
  final NetworkInfo networkInfo;

  ProjectsRepositoryImpl({@required this.allProjectsProvider, @required this.networkInfo}) : assert(allProjectsProvider != null), assert(networkInfo != null);

  @override
  Future<List<Suggestion>> projectsData() async {
    if(await networkInfo.isConnected) {
      final List<Suggestion> suggestionData = await allProjectsProvider.getAllProjects();
      return suggestionData;
    }
  }

  @override
  Future rating({int suggestionId, int value}) async {
    if(await networkInfo.isConnected) {
      await allProjectsProvider.putRating(suggestionId: suggestionId, value: value);
    }
  }

}