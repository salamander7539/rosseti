import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/screens/projects_screens/bloc/projects_bloc.dart';
import 'package:rosseti/screens/projects_screens/injections/projects_repository_di.dart';
import 'package:rosseti/screens/projects_screens/ui/project_screen_view.dart';
import 'package:rosseti/screens/projects_screens/ui/projects_screen_view.dart';

class ProjectScreen extends StatelessWidget {
  final int id;
  final String existingSolutionText;
  final String existingSolutionImage;
  final String existingSolutionVideo;
  final String proposedSolutionText;
  final String proposedSolutionImage;
  final String proposedSolutionVideo;
  final String positiveEffect;

  const ProjectScreen({Key key, this.id, this.existingSolutionText, this.existingSolutionImage, this.existingSolutionVideo, this.proposedSolutionText, this.proposedSolutionImage, this.proposedSolutionVideo, this.positiveEffect}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProjectsBlocImpl>(
      create: (context) => ProjectsBlocImpl(
        projectsRepository: ProjectsRepositoryInject.projectsRepository(),
      ),
      child: ProjectScreenView(id: id, existingSolutionText: existingSolutionText, existingSolutionImage: existingSolutionImage, existingSolutionVideo: existingSolutionVideo, proposedSolutionText: proposedSolutionText, proposedSolutionImage: proposedSolutionImage, proposedSolutionVideo: proposedSolutionVideo, positiveEffect: positiveEffect ,),
    );
  }
}
