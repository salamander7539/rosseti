import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/projects_screens/bloc/projects_bloc.dart';
import 'package:rosseti/screens/projects_screens/widgets/image_dowloader.dart';

class ProjectScreenView extends StatefulWidget {
  final int id;
  final String existingSolutionText;
  final String existingSolutionImage;
  final String existingSolutionVideo;
  final String proposedSolutionText;
  final String proposedSolutionImage;
  final String proposedSolutionVideo;
  final String positiveEffect;

  const ProjectScreenView({Key key,
    this.id,
    this.existingSolutionImage,
    this.existingSolutionText,
    this.existingSolutionVideo,
    this.proposedSolutionImage,
    this.proposedSolutionText,
    this.proposedSolutionVideo,
    this.positiveEffect})
      : super(key: key);

  @override
  _ProjectScreenViewState createState() =>
      _ProjectScreenViewState(

        existingSolutionImage: existingSolutionImage,
        existingSolutionText: existingSolutionText,
        existingSolutionVideo: existingSolutionVideo,
        proposedSolutionImage: proposedSolutionImage,
        proposedSolutionText: proposedSolutionText,
        proposedSolutionVideo: proposedSolutionVideo,
        positiveEffect: positiveEffect,
        id: id,
      );
}

class _ProjectScreenViewState extends State<ProjectScreenView> {
  final int id;
  final String existingSolutionText;
  final String existingSolutionImage;
  final String existingSolutionVideo;
  final String proposedSolutionText;
  final String proposedSolutionImage;
  final String proposedSolutionVideo;
  final String positiveEffect;

  _ProjectScreenViewState({this.existingSolutionImage,
    this.id,
    this.existingSolutionText,
    this.existingSolutionVideo,
    this.proposedSolutionImage,
    this.proposedSolutionText,
    this.proposedSolutionVideo,
    this.positiveEffect})
      : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 68.0, left: 20, right: 20, bottom: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: AppColor.mainColor,
                          size: 28,
                        ),
                      ),
                      Text(
                        'Проекты',
                        style: GoogleFonts.aBeeZee(
                          textStyle: TextStyle(
                              fontSize: 36.0, color: AppColor.mainColor),
                        ),
                      ),
                      Image.asset(
                        'assets/images/rosseti_logo.png',
                        height: 50.0,
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 14, bottom: 14.0),
                      child: Text(
                        'Сейчас так:',
                        style: GoogleFonts.aBeeZee(
                            color: AppColor.mainColor, fontSize: 20.0),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24.0),
                        color: Colors.white,
                      ),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * .85,
                      child: Text(
                        existingSolutionText,
                        style: GoogleFonts.aBeeZee(
                            textStyle: TextStyle(
                                color: AppColor.mainColor, fontSize: 20.0)),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: ImageDownload(
                            imageUrl: existingSolutionImage,
                          ),
                        ),
                        Container(
                          child: ImageDownload(
                            imageUrl: existingSolutionVideo,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 14, bottom: 14.0),
                      child: Text(
                        'Надо так:',
                        style: GoogleFonts.aBeeZee(
                            color: AppColor.mainColor, fontSize: 20.0),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24.0),
                        color: Colors.white,
                      ),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * .85,
                      child: Text(
                        proposedSolutionText,
                        style: GoogleFonts.aBeeZee(
                            textStyle: TextStyle(
                                color: AppColor.mainColor, fontSize: 20.0)),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: ImageDownload(
                            imageUrl: existingSolutionImage,
                          ),
                        ),
                        Container(

                          child: ImageDownload(
                            imageUrl: existingSolutionVideo,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 14, bottom: 14.0),
                      child: Text(
                        'И тогда будет так:',
                        style: GoogleFonts.aBeeZee(
                            color: AppColor.mainColor, fontSize: 20.0),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24.0),
                        color: Colors.white,
                      ),
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * .85,
                      child: Text(
                        positiveEffect,
                        style: GoogleFonts.aBeeZee(
                            textStyle: TextStyle(
                                color: AppColor.mainColor, fontSize: 20.0)),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding:
                  const EdgeInsets.only(left: 34, top: 14, bottom: 14.0),
                  child: Text(
                    'Оцените проект:',
                    style: GoogleFonts.aBeeZee(fontSize: 20.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 50.0),
                  child: Center(
                    child: Container(
                      child: RatingBar.builder(
                        itemSize: 40.0,
                        itemBuilder: (context, _) =>
                            Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                        initialRating: 0,
                        minRating: 1,
                        direction: Axis.horizontal,
                        itemCount: 5,
                        onRatingUpdate: (rating) {
                          BlocProvider.of<ProjectsBlocImpl>(context).add(RatingSendEvent(value: rating.round(), suggestionId: id));
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
