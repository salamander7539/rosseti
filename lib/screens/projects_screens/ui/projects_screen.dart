import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/screens/projects_screens/bloc/projects_bloc.dart';
import 'package:rosseti/screens/projects_screens/injections/projects_repository_di.dart';
import 'package:rosseti/screens/projects_screens/ui/projects_screen_view.dart';

class ProjectsScreen extends StatelessWidget {
  const ProjectsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProjectsBlocImpl>(
      create: (context) => ProjectsBlocImpl(
          projectsRepository: ProjectsRepositoryInject.projectsRepository(),
      ),
      child: ProjectsScreenView(),
    );
  }
}
