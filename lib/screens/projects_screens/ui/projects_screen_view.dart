import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/projects_screens/bloc/projects_bloc.dart';
import 'package:rosseti/screens/projects_screens/ui/project_screen.dart';
import 'package:rosseti/screens/projects_screens/ui/project_screen_view.dart';
import 'package:rosseti/screens/projects_screens/widgets/image_dowloader.dart';

class ProjectsScreenView extends StatefulWidget {
  const ProjectsScreenView({Key key}) : super(key: key);

  @override
  _ProjectsScreenViewState createState() => _ProjectsScreenViewState();
}

class _ProjectsScreenViewState extends State<ProjectsScreenView> {
  ProjectsBlocImpl projectsBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    projectsBloc = BlocProvider.of<ProjectsBlocImpl>(context);
    projectsBloc.add(FetchProjects());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: BlocBuilder<ProjectsBlocImpl, ProjectsState>(
          builder: (context, state) {
            if (state is ProjectsLoaded) {
              return Stack(
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 68.0, left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: Icon(
                                Icons.arrow_back_ios_rounded,
                                color: AppColor.mainColor,
                                size: 28,
                              ),
                            ),
                            Text(
                              'Проекты',
                              style: GoogleFonts.aBeeZee(
                                textStyle: TextStyle(
                                    fontSize: 36.0, color: AppColor.mainColor),
                              ),
                            ),
                            Image.asset(
                              'assets/images/rosseti_logo.png',
                              height: 50.0,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: LazyLoadScrollView(
                          onEndOfPage: () => projectsBloc.add(FetchProjects()),
                          scrollOffset: 70,
                          child: ListView.builder(
                            itemCount: state.suggestionList.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ProjectScreen(
                                          id: state.suggestionList[index].topicId,
                                          existingSolutionImage: state
                                              .suggestionList[index]
                                              .existingSolutionImage != null ? state
                                              .suggestionList[index]
                                              .existingSolutionImage : null,
                                          existingSolutionText: state
                                              .suggestionList[index]
                                              .existingSolutionText != null ? state
                                              .suggestionList[index]
                                              .existingSolutionText : null,
                                          existingSolutionVideo: state
                                              .suggestionList[index]
                                              .existingSolutionVideo != null ? state
                                              .suggestionList[index]
                                              .existingSolutionVideo : null,
                                          proposedSolutionImage: state
                                              .suggestionList[index]
                                              .proposedSolutionImage != null ? state
                                              .suggestionList[index]
                                              .proposedSolutionImage : null,
                                          proposedSolutionText: state
                                              .suggestionList[index]
                                              .proposedSolutionText != null ? state
                                              .suggestionList[index]
                                              .proposedSolutionText : null,
                                          proposedSolutionVideo: state
                                              .suggestionList[index]
                                              .proposedSolutionVideo != null ? state
                                              .suggestionList[index]
                                              .proposedSolutionVideo : null,
                                          positiveEffect: state
                                              .suggestionList[index]
                                              .positiveEffect != null ? state
                                              .suggestionList[index]
                                              .positiveEffect : null,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    height: 120,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12.0, horizontal: 8.0),
                                      child: ListTile(
                                        leading: Container(
                                            width: 60.0,
                                            height: 60.0,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12.0),
                                            ),
                                            child: ImageDownload(
                                              imageUrl: state
                                                  .suggestionList[index]
                                                  .existingSolutionImage,
                                            )),
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Expanded(
                                              child: Text(
                                                state.suggestionList[index].title != null ? state.suggestionList[index].title : '',
                                                style: GoogleFonts.aBeeZee(
                                                  textStyle: TextStyle(
                                                      color: AppColor.mainColor,
                                                      fontSize: 20.0),
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                textAlign: TextAlign.end,
                                              ),
                                            ),
                                          ],
                                        ),
                                        subtitle: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 16.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Column(
                                                children: [
                                                  Text(
                                                    state.suggestionList[index]
                                                        .author.fullName,
                                                    style: GoogleFonts.aBeeZee(
                                                      textStyle: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 14.0),
                                                    ),
                                                  ),
                                                  // Text(
                                                  //   state.suggestionList[index],
                                                  //   style: GoogleFonts.aBeeZee(
                                                  //     textStyle: TextStyle(
                                                  //         color: AppColor.mainColor,
                                                  //         fontSize: 14.0),
                                                  //   ),
                                                  // ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        isThreeLine: true,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            } else if (state is ProjectsErrorState) {
              return Center(
                child: Text(state.error),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
