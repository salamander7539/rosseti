import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ImageDownload extends StatelessWidget {
  final String imageUrl;
  const ImageDownload({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      placeholder: (context, url) => Container(),
      errorWidget: (context, url, error) => Container(),
    );
  }
}
