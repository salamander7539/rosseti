import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rosseti/core/locator.dart';
import 'package:rosseti/core/services/authentication_manager.dart';
import 'package:rosseti/screens/auth_screens/bloc/auth_bloc.dart';
import 'package:rosseti/screens/auth_screens/injections/auth_repository_di.dart';
import 'package:rosseti/screens/status_screen/ui/status_screen_view.dart';

class StatusScreen extends StatelessWidget {
  const StatusScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBlocImpl>(
      create: (context) => AuthBlocImpl(
        authenticationManager: locator.get<AuthenticationManager>(),
        authRepository: AuthRepositoryInject.authRepository(),
      ),
      child: StatusScreenView(),
    );
  }
}
