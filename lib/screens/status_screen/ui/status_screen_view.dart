import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/core/global/colors.dart';
import 'package:rosseti/screens/home_screen/ui/home_screen.dart';

class StatusScreenView extends StatefulWidget {
  const StatusScreenView({Key key}) : super(key: key);

  @override
  _StatusScreenViewState createState() => _StatusScreenViewState();
}

class _StatusScreenViewState extends State<StatusScreenView> {


  Widget row({String valueName, var value}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 40.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            valueName,
            style: GoogleFonts.aBeeZee(
              textStyle:
              TextStyle(fontSize: 20.0, color: AppColor.mainColor),
            ),
          ),
          Text(
            value != null ? value.toString() : '0',
            style: GoogleFonts.aBeeZee(
              textStyle:
              TextStyle(fontSize: 20.0, color: AppColor.mainColor),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 68.0, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 5.0,
                    ),
                    Text(
                      'Мой статус',
                      style: GoogleFonts.aBeeZee(
                        textStyle:
                        TextStyle(fontSize: 36.0, color: AppColor.mainColor),
                      ),
                    ),
                    Image.asset('assets/images/rosseti_logo.png', height: 50.0,)
                  ],
                ),
              ),
              SvgPicture.asset('assets/svg_images/crown.svg'),
              Text(
                'Серебрянный статус',
                style: GoogleFonts.aBeeZee(
                  textStyle:
                  TextStyle(fontSize: 20.0, color: AppColor.mainColor),
                ),
              ),
              row(valueName: 'Оценок'),
              row(valueName: 'Комментариев'),
              row(valueName: 'Предложений'),
              row(valueName: 'Одобрено'),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 140.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(24.0)),
                  ),
                  shadowColor: Colors.grey.withOpacity(0.8),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width * .8,
                  height: 58.0,
                  child: Center(
                    child: Text('Готово', style: GoogleFonts.aBeeZee(fontSize: 20.0, color: AppColor.mainColor),),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
