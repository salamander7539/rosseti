import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rosseti/screens/auth_screens/ui/welcome_screen.dart';
import 'package:rosseti/screens/home_screen/ui/home_screen.dart';
import 'core/global/colors.dart';
import 'core/locator.dart';
import 'core/services/authentication_manager.dart';


class SplashScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.black);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final splashDelay = 2;
  bool token = false;

  @override
  void initState() {
    super.initState();
    getToken();
    _loadWidget();
  }

  getToken() async {
    token = await locator.get<AuthenticationManager>().getUserToken();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => token != null ? HomeScreen() : WelcomeScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Image.asset(
                  'assets/images/rosseti_logo.png',
                  height: 200.0,
                ),
                Text(
                  'seti.inno',
                  style: GoogleFonts.aBeeZee(
                    textStyle: TextStyle(color: AppColor.mainColor),
                    fontSize: 62.0,
                  ),
                ),
                Text(
                  'Рационализатор',
                  style: GoogleFonts.aBeeZee(fontSize: 20.0),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
